#!/system/bin/sh
#
# 3G TurboCharger created by zeppelinrox.
#
busybox mount -o remount,rw / 2>/dev/null
busybox mount -o remount,rw rootfs 2>/dev/null
if [ ! -d "/sqlite_stmt_journals" ]; then
	mkdir /sqlite_stmt_journals
	madesqlitefolder=1
else madesqlitefolder=0
fi
line=================================================
cat > /sdcard/!3GTurboCharger.html <<EOF
<br>
<br>
<i><u><b>The 3G TurboCharger</b></u> by zeppelinrox.</i><br>
<br>
Homework/Background Info... see the folllowing links for resources used.<br>
<br>
<a href="http://forum.xda-developers.com/showthread.php?t=595108">XDA Thread 1</a><br>
<a href="http://forum.xda-developers.com/showthread.php?t=924440">XDA Thread 2</a><br>
<a href="http://forum.xda-developers.com/showpost.php?p=5123531&postcount=46">XDA Thread 3</a><br>
<a href="http://www.nickshertzer.com/wordpress/?p=600">And a blog...</a><br>
<br>
Wi-Fi AfterBurner.<br>
<br>
<a href="http://forum.xda-developers.com/showthread.php?t=1019371">XDA Thread (Faster WIFI - N Standard Enabling)</a><br>
<br>
	The 3g build.prop tweaks aren't really anything new... but I did put a new spin on it!  ;-)<br>
<br>
	You can find various individual configurations but as far as I know, my script is the only one that lets the user choose from more than one configuration.<br>
<br>
<b>Options 1 and 2</b> aren't anything out of the ordinary... they are pretty much typical 3g tweaks. I may have tweaked a couple of values in each one.<br>
<br>
<b>Option 3</b> has maximum values from initial/older 3g tweaks.<br>
<br>
<b>Option 4</b> has maximum values from newer 3g tweaks and personal research.<br>
<br>
<b>Option 5</b> has mximum, perhaps unconfirmed maximum values that aren't very well known.<br>
	For example, googling hsxpa=5 or gprsclass=34 won't give many results. heh.<br>
<br>
<b>Option 6</b> is pretty cool and came about by accident - nulled values.<br>
	I made a scripting error on a beta/test and all options applied no values so you had gprsclass= and hsxpa= etc etc... (oops)<br>
	But many users reported great results (uh... I knew that would happen... honest!)<br>
	So I figured, in theory, that could make sense too - instead of applying maximum values, apply NO values and let the OS figure it out and without limitation (the more you know... lol)<br>
<br>
	So, I pretty much came up with configurations/options 3 to 6 based on research and user results (from the XDA SuperCharger thread) :D<br>
<br>
<br>
<u><b>Option 1 - QUICK!</b></u><br>
<br>
ro.ril.hsxpa=2<br>
ro.ril.gprsclass=10<br>
ro.ril.hep=1<br>
ro.ril.hsdpa.category=8<br>
ro.ril.enable.3g.prefix=1<br>
ro.ril.htcmaskw1.bitmask=4294967295<br>
ro.ril.htcmaskw1=14449<br>
ro.ril.hsupa.category=6<br>
ro.ril.def.agps.mode=2<br>
ro.ril.def.agps.feature=1<br>
ro.ril.enable.sdr=1<br>
ro.ril.enable.gea3=1<br>
ro.ril.enable.fd.plmn.prefix=23402,23410,23411<br>
ro.ril.disable.power.collapse=0<br>
ro.ril.enable.a52=0<br>
ro.ril.enable.a53=0<br>
ro.ril.enable.dtm=0<br>
<br>
<u><b>Option 2 - FAST!?</b></u><br>
<br>
ro.ril.hsxpa=2<br>
ro.ril.gprsclass=12<br>
ro.ril.hep=1<br>
ro.ril.hsdpa.category=8<br>
ro.ril.enable.3g.prefix=1<br>
ro.ril.htcmaskw1.bitmask=4294967295<br>
ro.ril.htcmaskw1=14449<br>
ro.ril.hsupa.category=6<br>
ro.ril.def.agps.mode=2<br>
ro.ril.def.agps.feature=1<br>
ro.ril.enable.sdr=1<br>
ro.ril.enable.gea3=1<br>
ro.ril.enable.fd.plmn.prefix=23402,23410,23411<br>
ro.ril.disable.power.collapse=0<br>
ro.ril.enable.a52=0<br>
ro.ril.enable.a53=0<br>
ro.ril.enable.dtm=0<br>
<br>
<u><b>Option 3 - FASTER!?</b></u> (From Update 2)<br>
<br>
ro.ril.hsxpa=2<br>
ro.ril.gprsclass=12<br>
ro.ril.hep=1<br>
ro.ril.hsdpa.category=28<br>
ro.ril.enable.3g.prefix=1<br>
ro.ril.htcmaskw1.bitmask=4294967295<br>
ro.ril.htcmaskw1=14449<br>
ro.ril.hsupa.category=9<br>
ro.ril.def.agps.mode=2<br>
ro.ril.def.agps.feature=1<br>
ro.ril.enable.sdr=1<br>
ro.ril.enable.gea3=1<br>
ro.ril.enable.fd.plmn.prefix=23402,23410,23411<br>
ro.ril.disable.power.collapse=1<br>
ro.ril.enable.a52=0<br>
ro.ril.enable.a53=1<br>
ro.ril.enable.dtm=1<br>
<br>
<u><b>Option 4 - FASTEST!?</b></u> (From Update 3 test 8)<br>
<br>
ro.ril.hsxpa=3<br>
ro.ril.gprsclass=32<br>
ro.ril.hep=1<br>
ro.ril.hsdpa.category=28<br>
ro.ril.enable.3g.prefix=1<br>
ro.ril.htcmaskw1.bitmask=4294967295<br>
ro.ril.htcmaskw1=268449905<br>
ro.ril.hsupa.category=9<br>
ro.ril.def.agps.mode=2<br>
ro.ril.def.agps.feature=1<br>
ro.ril.enable.sdr=1<br>
ro.ril.enable.gea3=1<br>
ro.ril.enable.fd.plmn.prefix=23402,23410,23411<br>
ro.ril.disable.power.collapse=1<br>
ro.ril.enable.a52=0<br>
ro.ril.enable.a53=1<br>
ro.ril.enable.dtm=1<br>
<br>
<u><b>Option 5 - EXPERIMENTAL 1</b></u><br>
<br>
ro.ril.hsxpa=5<br>
ro.ril.gprsclass=34<br>
ro.ril.hep=1<br>
ro.ril.hsdpa.category=28<br>
ro.ril.enable.3g.prefix=1<br>
ro.ril.htcmaskw1.bitmask=4294967295<br>
ro.ril.htcmaskw1=268449905<br>
ro.ril.hsupa.category=9<br>
ro.ril.def.agps.mode=2<br>
ro.ril.def.agps.feature=1<br>
ro.ril.enable.sdr=1<br>
ro.ril.enable.gea3=1<br>
ro.ril.enable.fd.plmn.prefix=23402,23410,23411<br>
ro.ril.disable.power.collapse=1<br>
ro.ril.enable.a52=1<br>
ro.ril.enable.a53=1<br>
ro.ril.enable.dtm=1<br>
<br>
<u><b>Option 6 - EXPERIMENTAL 2</b></u><br>
<br>
ro.ril.hsxpa=<br>
ro.ril.gprsclass=<br>
ro.ril.hep=1<br>
ro.ril.hsdpa.category=<br>
ro.ril.enable.3g.prefix=1<br>
ro.ril.htcmaskw1.bitmask=<br>
ro.ril.htcmaskw1=<br>
ro.ril.hsupa.category=<br>
ro.ril.def.agps.mode=2<br>
ro.ril.def.agps.feature=1<br>
ro.ril.enable.sdr=1<br>
ro.ril.enable.gea3=1<br>
ro.ril.enable.fd.plmn.prefix=23402,23410,23411<br>
ro.ril.disable.power.collapse=1<br>
ro.ril.enable.a52=<br>
ro.ril.enable.a53=<br>
ro.ril.enable.dtm=<br>
<br>
For more help and info,<br>
See the <a href="http://goo.gl/qM6yR">-=V6 SuperCharger Thread=-</a><br>
Feedback is Welcome!<br>
<br>
-=zeppelinrox=- @ <a href="http://goo.gl/qM6yR">XDA</a> & <a href="http://www.droidforums.net/forum/droid-hacks/148268-script-v6-supercharger-htk-bulletproof-launchers-fix-memory-all-androids.html">Droid</a> Forums<br>
<br>
EOF
if [ "$madesqlitefolder" -eq 1 ]; then rm -r /sqlite_stmt_journals; fi
busybox mount -o remount,ro / 2>/dev/null
busybox mount -o remount,ro rootfs 2>/dev/null
speed=2
sleep="sleep $speed"
clear
echo ""
echo $line
echo " NOTE: BUSYBOX v1.16.2 OR HIGHER IS RECOMMENDED!"
echo $line
echo ""
sleep 3
clear;echo "";echo "";echo "  The...                                       |";echo "";echo "";echo "";echo "";echo "";$sleep
clear;echo "";echo "";echo "| The...                                       |";echo "|               3                              |";echo "";echo ""; echo "                                       zoom... |";echo "";$sleep
clear;echo "";echo "";echo "| The...                                       |";echo "|               3G                             |";echo "";echo "";echo "| zoom...                                      |";echo "";$sleep
clear;echo "";echo "";echo "| The...                                       |";echo "|               3G TurboCharger                |";echo "";echo ""; echo "| zoom...                              zoom... |";echo "";$sleep
clear;echo "";echo "";echo "| The...                                       |";echo "|             -=3G TurboCharger=-              |";echo "| by:                                          |";echo "";echo "| zoOM...                              zoOM... |";echo "";$sleep
clear;echo "";echo $line;echo "| The...                                       |";echo "|             -=3G TurboCharger=-              |";echo "| by:                                          |";echo "|               -=zeppelinrox=-                |";echo "| zOOM...                              zOOM... |"
if [ ! "`id | grep =0`" ]; then
	echo $line
	$sleep
	echo ""
	echo " You are NOT running this script as root..."
	echo ""
	sleep 4
	echo $line
	echo "                      ...No SuperUser for you!!"
	echo $line
	echo ""
	sleep 4
	echo "     ...Please Run as Root and try again..."
	echo ""
	sleep 4
	exit 69
fi
while :; do
 echo $line
 $sleep
 echo " View settings in /sdcard/!3GTurboCharger.html!"
 echo $line
 busybox echo " \\\\\\\\ T H E  3 G   T U R B O C H A R G E R ////"
 echo "  ============================================"
 echo "  { 1. Quick!               {Battery Friendly}"
 echo "  { 2. Fast!?                {\"Normal\" Values}"
 echo "  { 3. Faster!?             {Nearly Maxed Out}"
 echo "  { 4. Fastest!?             {Known Maxed Out}"
 echo "  { 5. Experimental 1     {\"Secret\" Maxed Out}"
 echo "  { 6. Experimental 2          {Nulled Values}"
 echo "  { 7. Wi-Fi AfterBurner       {Tiwlan Tweaks}"
 echo "  { 8. UnTurboCharger     {Revert 3G Settings}"
 echo "  { 9. Wi-Fixer              {Possible Repair}"
 echo "  {10. Get SpeedTest.Net App!   {Opens Market}"
 echo "  {11. Reboot                          {Poof!}"
 echo "  {12. Exit                          {Buh Bye}"
 echo "  ============================================"
 busybox echo "   \\\\\\\\           Update 3 RC6           ////"
 echo "    ========================================"
 if [ "`grep ro.cdma /system/build.prop`" ]; then
	 busybox echo "        \\\\\\\\  CDMA Mode Activated!  ////"
	 echo "         =============================="
 else
	 busybox echo "        \\\\\\\\   GSM Mode Activated!  ////"
	 echo "         =============================="
 fi
 echo ""
 echo " Settings are applied at the top of build.prop!"
 echo ""
 echo $line
 if [ "`grep "3G Settings" /system/build.prop`" ]; then
	echo "         `grep "3G Settings" /system/build.prop | sed '/3G Settings/s/#//'` Found!"
 else echo "        No 3G TurboCharger Settings Found!"
 fi
 echo $line
 echo ""
 echo "    Your Mileage WILL Vary So Test Them All!"
 echo ""
 echo " The SpeedTest.Net app is highly recommended :)"
 echo ""
 for b in `busybox find /system -iname tiwlan*.ini`; do
	if [ "`grep 'Single_Dual_Band_Solution = 1' $b`" ]; then burner=yep; break; fi
 done
 if [ "$burner" ]; then
	echo "   ==========================================="
	busybox echo "  ////   Wi-Fi AfterBurner is Installed!   \\\\\\\\"
 else
	echo "   ==========================================="
	busybox echo "  //// Wi-Fi AfterBurner is NOT Installed! \\\\\\\\"
 fi
 echo " ==============================================="
 if [ "$setting" ]; then
	busybox echo "  \\\\\\\\  SETTINGS CHANGE REQUIRES A REBOOT  ////"
	echo "   ==========================================="
 fi
 echo -n "         Please Enter Option [1 - 12]: "
 read turboopt
 echo $line
 busybox echo "            \\\\\\\\ 3G TURBOCHARGER ////"
 echo "             ======================="
 echo ""
 $sleep
 if [ "$turboopt" -lt 11 ] 2>/dev/null; then
	busybox mount -o remount,rw / 2>/dev/null
	busybox mount -o remount,rw rootfs 2>/dev/null
	busybox mount -o remount,rw /system 2>/dev/null
	busybox mount -o remount,rw `busybox mount | grep system | awk '{print $1,$3}' | sed -n 1p` 2>/dev/null
	if [ ! -d "/sqlite_stmt_journals" ]; then
		mkdir /sqlite_stmt_journals
		madesqlitefolder=1
	else madesqlitefolder=0
	fi
 fi
 case $turboopt in
  1) echo "                      QUICK!"
	 tcconfig="Quick!";hsxpa=2;gprsclass=10;hsdpa=8;bitmask=4294967295;htcmaskw1=14449;hsupa=6;dpc=0;afivetwo=0;afivethree=0;dtm=0;;
  2) echo "                      FAST!?"
	 tcconfig="Fast!?"; hsxpa=2;gprsclass=12;hsdpa=8;bitmask=4294967295;htcmaskw1=14449;hsupa=6;dpc=0;afivetwo=0;afivethree=0;dtm=0;;
  3) echo "                     FASTER!?"
	 tcconfig="Faster!?";hsxpa=2;gprsclass=12;hsdpa=28;bitmask=4294967295;htcmaskw1=14449;hsupa=9;dpc=1;afivetwo=0;afivethree=1;dtm=1;;
  4) echo "                     FASTEST!?"
	 tcconfig="Fastest!?";hsxpa=3;gprsclass=32;hsdpa=28;bitmask=4294967295;htcmaskw1=268449905;hsupa=9;dpc=1;afivetwo=0;afivethree=1;dtm=1;;
  5) echo "                 EXPERIMENTAL 1!"
	 tcconfig="Experimental_1";hsxpa=5;gprsclass=34;hsdpa=28;bitmask=4294967295;htcmaskw1=268449905;hsupa=9;dpc=1;afivetwo=1;afivethree=1;dtm=1;;
  6) echo "                 EXPERIMENTAL 2!"
	 tcconfig="Experimental_2";hsxpa=;gprsclass=;hsdpa=;bitmask=;htcmaskw1=;hsupa=;dpc=1;afivetwo=;afivethree=;dtm=;;
  7) echo "           =========================="
	 busybox echo "          //// WI-FI AfterBurner! \\\\\\\\"
	 echo $line
	 echo ""
	 echo " Your tiwlan.ini file will be tweaked like so:"
	 echo ""
	 $sleep
	 echo "                 HT_Enable = 1"
	 echo "           BurstModeEnable = 1"
	 echo "                 WiFiAdhoc = 1"
	 echo " Single_Dual_Band_Solution = 1"
	 echo ""
	 $sleep
	 echo "    Most of these are set to 0 by default :P"
	 echo ""
	 $sleep
	 echo " You may need to also configure your router..."
	 echo ""
	 $sleep
	 echo " If it has the 5GHz band in it's options..."
	 echo ""
	 $sleep
	 echo "  ...enable the 5GHz if it's now set to 2.4GHz!"
	 echo ""
	 $sleep
	 echo $line
	 echo " See /sdcard/!3GTurboCharger.html for more info!"
	 echo $line
	 echo ""
	 $sleep
	 while :; do
		echo -n " Enter Wi-Fi (A)fterBurner, (U)n-Burn, E(X)it:"
		read wb
		echo ""
		echo $line
		case $wb in
		  a|A)setting="changed"
			  for i in `busybox find /system -iname "tiwlan*.ini"`; do
				echo ""
				$sleep
				echo " Found $i!"
				echo ""
				$sleep
				if [ -f "$i.un3g" ]; then echo " Leaving ORIGINAL ${i##*/} backup intact..."
				else
					echo " Backing up ORIGINAL ${i##*/}..."
					echo ""
					$sleep
					cp -r $i $i.un3g
					echo "    ...as $i.un3g!"
				fi
				echo ""
				$sleep
				echo $line
				echo " AfterBurning $i..."
				echo $line
				sed -i '/HT_Enable=/s/e= *./e=1/' $i
				sed -i '/BurstModeEnable =/s/e = *./e = 1/' $i
				sed -i '/WiFiAdhoc =/s/c = *./c = 1/' $i
				sed -i '/Single_Dual_Band_Solution =/s/n = *./n = 1/' $i
			  done
			  $sleep
			  echo "           Wi-Fi AfterBurner Complete!"
			  break;;
		  u|U)setting="changed"
			  burner=
			  if [ "`busybox find /system -iname tiwlan*.un3g`" ]; then
				for i in `busybox find /system -iname "tiwlan*.ini"`; do
					if [ -f "$i.un3g" ]; then
						echo ""
						$sleep
						echo " Restoring ORIGINAL ${i##*/}..."
						echo ""
						$sleep
						mv $i.un3g $i
						echo $line
					fi
				done
				echo "                 Wi-Fi Un-Burned!"
			  else echo "     Can't Un-Burn Wi-Fi! No Backups Found!"
			  fi
			  break;;
		  x|X)echo " Returning to main menu..."
			  break;;
			*)echo "      Invalid entry... Please try again :)"
			  echo $line
			  echo ""
			  $sleep;;
		esac
	 done;;
  8) echo "             ======================"
	 busybox echo "            //// UNTURBOCHARGER \\\\\\\\";;
  9) echo "                ================"
	 busybox echo "               //// WI-FIXER \\\\\\\\"
	 echo $line
	 echo ""
	 echo " If Wi-Fi doesn't work after using..."
	 echo ""
	 $sleep
	 echo "                ...The 3G TurboCharger..."
	 echo ""
	 $sleep
	 echo "                          ...this MIGHT fix it!"
	 echo ""
	 $sleep
	 echo " This will simply rename wpa_supplicant.conf..."
	 echo ""
	 $sleep
	 echo "               ...so that a new one is created!"
	 echo ""
	 $sleep
	 echo " First, Turn Wifi OFF..."
	 echo ""
	 $sleep
	 echo "            ...Apply The WiFi-xer..."
	 echo ""
	 $sleep
	 echo "                          ...then Turn WiFi ON!"
	 echo ""
	 $sleep
	 echo $line
	 echo " Note: You will need to re-enter WiFi passwords!"
	 echo $line
	 echo ""
	 $sleep
	 echo " This will overwrite previously renamed file!"
	 echo ""
	 $sleep
	 echo " Un-Wi-Fixer will restore the renamed file!"
	 echo ""
	 while :; do
		echo $line
		echo ""
		$sleep
		echo -n " Enter Wi-(F)ixer, (U)n-Wi-Fixer, E(X)it: "
		read wifixer
		echo ""
		echo $line
		case $wifixer in
		  f|F)if [ -f "/d*/misc/wifi/wpa_supplicant.conf" ]; then
				mv /d*/misc/wifi/wpa_supplicant.conf /d*/misc/wifi/wpa_supplicant.conf.un3g
				echo " Current wpa_supplicant.conf was saved..."
				echo ""
				$sleep
				echo " ...as /*data/misc/wifi/wpa_supplicant.conf.un3g"
			  else echo " oops... did NOT find wpa_supplicant.conf!"
			  fi
			  echo $line
			  echo ""
			  $sleep
			  echo " Another Tip: Unplug and Replug your router...."
			  echo ""
			  $sleep
			  echo "                    ....before turning on WiFi!"
			  echo ""
			  break;;
		  u|U)if [ -f "/d*/misc/wifi/wpa_supplicant.conf.un3g" ]; then
				cp -fr /d*/misc/wifi/wpa_supplicant.conf.un3g /d*/misc/wifi/wpa_supplicant.conf
				echo " wpa_supplicant.conf was restored!"
			  else echo " oops... did NOT find wpa_supplicant.conf.un3g!"
			  fi
			  break;;
		  x|X)echo " Returning to main menu..."
			  break;;
			*)echo "      Invalid entry... Please try again :)";;
		esac
	 done;;
 10) echo $line
	 echo ""
	 echo " Loading Market..."
	 echo ""
	 $sleep
	 su -c "LD_LIBRARY_PATH=/vendor/lib:/system/lib am start http://market.android.com/details?id=org.zwanoo.android.speedtest"
	 echo "";;
 11) echo $line
	 echo "                    !!POOF!!"
	 echo $line
	 echo ""
	 sleep 2
	 busybox sync
	 echo 1 > /proc/sys/kernel/sysrq 2>/dev/null
	 echo b > /proc/sysrq-trigger 2>/dev/null
	 echo "  If it don't go poofie, just reboot manually!"
	 echo ""
	 busybox reboot;;
 12) echo " Did you find this useful? Feedback is welcome!";;
  *) echo "      Invalid entry... Please try again..."
	 echo ""
	 sleep 2
	 echo "            1 <= Valid Option => 12 !!"
	 echo ""
	 sleep 2
	 echo -n "      Press the Enter Key to continue... ;)"
	 read enterKey
	 echo ""
	 turboopt=0;;
 esac
 if [ "$turboopt" -ge 1 ] && [ "$turboopt" -le 12 ]; then
	echo $line
	echo ""
	$sleep
	if [ "$turboopt" -eq 8 ] && [ ! "`grep "3G Settings" /system/build.prop`" ]; then echo "  Nice Try! No 3G TurboCharger Settings Found!"
	elif [ "$turboopt" -le 8 ] && [ "$turboopt" -ne 7 ]; then
		if [ "$turboopt" -ne 8 ]; then
			if [ -f "/system/build.prop.unsuper" ]; then
				echo " Leaving ORIGINAL build.prop backup intact..."
			else
				echo " Backing up ORIGINAL build.prop..."
				echo ""
				$sleep
				cp -r /system/build.prop /system/build.prop.unsuper
				echo "              ...as /system/build.prop.unsuper!"
			fi
			if [ -f "/system/bin/build.prop" ] && [ ! -f "/system/bin/build.prop.unsuper" ]; then cp -r /system/bin/build.prop /system/bin/build.prop.unsuper; fi
			$sleep
		else
			echo -n "         :|"
			sleep 3
			echo -n "    !@#?&%(*)(*)&(!)?!"
			sleep 3
			echo "    :/"
			sleep 3
		fi
		echo ""
		sed -i '/TurboCharger by/,/TurboCharged/d' /system/build.prop
		if [ "$turboopt" -ne 8 ]; then
			sed -i '2 a\
# 3G TurboCharger by zeppelinrox.\
#\
# DO NOT DELETE COMMENTS. DELETING COMMENTS WILL BREAK UNINSTALL ROUTINE!\
#\
# Homework/Background Info... see the following links for resources used.\
# http://forum.xda-developers.com/showthread.php?t=595108\
# http://forum.xda-developers.com/showthread.php?t=924440\
# http://forum.xda-developers.com/showpost.php?p=5123531&postcount=46\
# http://www.nickshertzer.com/wordpress/?p=600\
#\
# '$tcconfig' 3G Settings\
#\
ro.ril.hsxpa='$hsxpa'\
ro.ril.gprsclass='$gprsclass'\
ro.ril.hep=1\
ro.ril.hsdpa.category='$hsdpa'\
ro.ril.enable.3g.prefix=1\
ro.ril.htcmaskw1.bitmask='$bitmask'\
ro.ril.htcmaskw1='$htcmaskw1'\
ro.ril.hsupa.category='$hsupa'\
ro.ril.enable.sdr=1\
ro.ril.enable.gea3=1\
ro.ril.enable.fd.plmn.prefix=23402,23410,23411\
ro.ril.disable.power.collapse='$dpc'\
ro.ril.def.agps.mode=2\
ro.ril.def.agps.feature=1         # GSM=1, CDMA=2\
ril.cdma.ppp.up=                  # Credit and Thanks to metalspring at XDA\
persist.ril.uart.flowctrl=10      # Credit and Thanks to motcher41 at XDA\
ro.mot.eri.losalert.delay=2000\
ro.ril.enable.a52='$afivetwo'\
ro.ril.enable.a53='$afivethree'\
ro.ril.enable.dtm='$dtm'\
# To fix connection issues, change "enable.a53" and "enable.dtm" to be =0\
# End of 3G TurboCharged Entries.' /system/build.prop
			if [ "`grep ro.cdma /system/build.prop`" ]; then sed -i 's/agps.feature=1/agps.feature=2/' /system/build.prop; fi
			if [ ! "`grep SuperCharged /system/build.prop`" ] && [ ! "`grep SuperCharged /data/local.prop`" ];then sed -i '/fix connection/ a\
#\
net.tcp.buffersize.default=6144,87380,1048576,6144,87380,524288\
net.tcp.buffersize.wifi=524288,1048576,2097152,524288,1048576,2097152\
net.tcp.buffersize.lte=524288,1048576,2097152,524288,1048576,2097152\
net.tcp.buffersize.hsdpa=6144,87380,1048576,6144,87380,1048576\
net.tcp.buffersize.evdo_b=6144,87380,1048576,6144,87380,1048576\
net.tcp.buffersize.umts=6144,87380,1048576,6144,87380,524288\
net.tcp.buffersize.gprs=6144,87380,1048576,6144,87380,524288\
net.tcp.buffersize.edge=6144,87380,524288,6144,16384,262144\
net.tcp.buffersize.hspa=6144,87380,524288,6144,16384,262144\
#' /system/build.prop
			fi 2>/dev/null
			echo $line
			echo " $tcconfig Settings installed..."
		else
			echo $line
			echo " UnTurboCharging Complete..."
		fi
		echo $line
		echo ""
		$sleep
		echo "                            ...Reboot Required!"
		setting="changed"
		if [ -f "/system/bin/build.prop" ]; then cp -fr /system/build.prop /system/bin; fi
#		chmod 644 /system/build.prop
#		chmod 644 /system/bin/build.prop
	fi
	if [ "$turboopt" -le 8 ] && [ "$turboopt" -ne 7 ]; then
		echo ""
		echo $line
		echo ""
		$sleep
	fi
	echo " The 3G TurboCharger Installer..."
	echo ""
	$sleep
	echo "     ...Wi-Fi AfterBurner and WiFi-xer..."
	echo ""
	$sleep
	echo "     ...by -=zeppelinrox=- @ XDA & Droid Forums"
	echo ""
	if [ "$turboopt" -eq 12 ]; then
		$sleep
		echo "                                     Buh Bye :)"
		echo ""
		echo $line
		echo ""
		$sleep
		exit 0
	fi
	if [ "$madesqlitefolder" -eq 1 ]; then rm -r /sqlite_stmt_journals; fi
	busybox mount -o remount,ro / 2>/dev/null
	busybox mount -o remount,ro rootfs 2>/dev/null
	busybox mount -o remount,ro /system 2>/dev/null
	busybox mount -o remount,ro `busybox mount | grep system | awk '{print $1,$3}' | sed -n 1p` 2>/dev/null
 fi
done
